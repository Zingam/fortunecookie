#include "s3eDevice.h"

#include "IwUI.h"

#include "constants/Constants.h"
#include "Generator.h"

CIwUIElement* gScreen;

CIwUIElement* pScreen_FortuneCookie_Main;
CIwUIElement* pScreen_FortuneCookie_Show;

const char* getZodiacText(CookieType cookieType)
{
	switch (cookieType)
	{
	case CookieOfTheDay: 
		return "Cookie of the Day";
	case Friendship: 
		return "Friendship";
	case Happiness: 
		return "Happiness";
	case Love: 
		return "Love";
	case Wisdom: 
		return "Wisdom";
	}

	return "Error: unknown";
}

void SetScreenFortuneCookie_Show(CookieType cookieType, CIwSVec2 uv0, CIwSVec2 uv1)
{
	CIwUILabel* labelTitleBar = IwSafeCast<CIwUILabel*>(pScreen_FortuneCookie_Show->GetChildNamed("TitleBar_Label"));

	labelTitleBar->SetCaption(getZodiacText(cookieType));

	CIwUILabel* label = IwSafeCast<CIwUILabel*>(pScreen_FortuneCookie_Show->GetChildNamed("Label_FortuneText"));
	Generator generator;
	label->SetCaption(generator.makePrediction(cookieType));

	pScreen_FortuneCookie_Main->SetVisible(false);
	pScreen_FortuneCookie_Show->SetVisible(true);
};

class CController : public CIwUIController
{
public:
    CController()
    {
        IW_UI_CREATE_VIEW_SLOT1(this, "CController", CController, OnClick_CookieOfTheDay, CIwUIElement*)
        IW_UI_CREATE_VIEW_SLOT1(this, "CController", CController, OnClick_Friendship, CIwUIElement*)
		IW_UI_CREATE_VIEW_SLOT1(this, "CController", CController, OnClick_Happiness, CIwUIElement*)
		IW_UI_CREATE_VIEW_SLOT1(this, "CController", CController, OnClick_Love, CIwUIElement*)
		IW_UI_CREATE_VIEW_SLOT1(this, "CController", CController, OnClick_Wisdom, CIwUIElement*)

		IW_UI_CREATE_VIEW_SLOT1(this, "CController", CController, OnClick_Close, CIwUIElement*)
    }

private:
    void OnClick_CookieOfTheDay(CIwUIElement* Clicked)
    {
		CookieType cookieType = CookieOfTheDay;

		SetScreenFortuneCookie_Show(cookieType, CIwSVec2(632, 2), CIwSVec2(837, 197));
    }

	void OnClick_Friendship(CIwUIElement* Clicked)
    {
		CookieType cookieType = Friendship;

		SetScreenFortuneCookie_Show(cookieType, CIwSVec2(2, 200), CIwSVec2(208, 395));
    }

	void OnClick_Happiness(CIwUIElement* Clicked)
    {
		CookieType cookieType = Happiness;

		SetScreenFortuneCookie_Show(cookieType, CIwSVec2(211, 200), CIwSVec2(418, 395));
	}

	void OnClick_Love(CIwUIElement* Clicked)
    {
		CookieType cookieType = Love;

		SetScreenFortuneCookie_Show(cookieType, CIwSVec2(421, 200), CIwSVec2(628, 395));
	}

	void OnClick_Wisdom(CIwUIElement* Clicked)
    {
		CookieType cookieType = Wisdom;

		SetScreenFortuneCookie_Show(cookieType, CIwSVec2(632, 200), CIwSVec2(837, 395));
	}

	void OnClick_Close(CIwUIElement* Clicked)
    {
		pScreen_FortuneCookie_Main->SetVisible(true);
		pScreen_FortuneCookie_Show->SetVisible(false);
	}
};

void SetScreen(const char* screen)
{
    //Find the dialog template
    CIwUIElement* pDialogTemplate = (CIwUIElement*)IwGetResManager()->GetResNamed(screen, "CIwUIElement");

    if (!pDialogTemplate)
        return;

    //Remove the old screen
    if (gScreen)
        IwGetUIView()->DestroyElements();

    //And instantiate it
    gScreen = pDialogTemplate->Clone();
    IwGetUIView()->AddElement(gScreen);
    IwGetUIView()->AddElementToLayout(gScreen);
}

int main() 
{
	IwUIInit();
	new CIwUIView();
	new CController();
	
	s3eDebugOutputString("Initializing...");

	//Load the group containing the example font
    IwGetResManager()->LoadGroup("FortuneCookieUI.group");

	pScreen_FortuneCookie_Main = CIwUIElement::CreateFromResource("Screen_FortuneCookie_Main");
	pScreen_FortuneCookie_Show = CIwUIElement::CreateFromResource("Screen_FortuneCookie_Show");

	IwGetUIView()->AddElement(pScreen_FortuneCookie_Main);
	IwGetUIView()->AddElement(pScreen_FortuneCookie_Show);

	pScreen_FortuneCookie_Main->SetVisible(true);
	pScreen_FortuneCookie_Show->SetVisible(false);

	IwGetUIView()->AddElementToLayout(pScreen_FortuneCookie_Main);
	IwGetUIView()->AddElementToLayout(pScreen_FortuneCookie_Show);
	
	const int32 frametime = 25;


	
	while (!s3eDeviceCheckQuitRequest())
	{
		s3eDeviceYield(0);
        s3eKeyboardUpdate();

		IwGxClear(IW_GX_COLOUR_BUFFER_F | IW_GX_DEPTH_BUFFER_F);

		//Render Debugging outlines of the element hierarchy
		IwUIDebugRender(NULL, IW_UI_DEBUG_LAYOUT_OUTLINE_F | IW_UI_DEBUG_ELEMENT_OUTLINE_F);
		
		IwGetUIController()->Update();
		IwGetUIView()->Update(frametime);
		
		IwGetUIView()->Render();
		
		IwGxFlush();
		IwGxSwapBuffers();
	}

	delete IwGetUIController();
	delete IwGetUIView();

	IwUITerminate();

	return 0;
}