#ifndef WISDOM_H
#define WISDOM_H

static const char* stringsWisdom[] = {
    "Nothing is more expensive than a missed opportunity.",
    "When you find your path, you must not be afraid. You need to have sufficient courage to make mistakes.",
    "The two worst strategic mistakes to make are acting prematurely and letting an opportunity slip; to avoid this, the warrior treats each situation as if it were unique and never resorts to formulae, recipes or other people\'s opinions.",
    "There is suffering in life, and there are defeats. No one can avoid them. But it\'s better to lose some of the battles in the struggles for your dreams than to be defeated without ever knowing what you\'re fighting for.",
    "It\'s the simple things in life that are the most extraordinary; only wise men are able to understand them.",
    "At a certain point in our lives, we lose control of what\'s happening to us, and our lives become controlled by fate. That\'s the world\'s greatest lie.",
    "When you want something, all the universe conspires in helping you to achieve it.",
    "Do not go where the path may lead, go instead where there is no path and leave a trail.",
    "Experience is not what happens to you; it\'s what you do with what happens to you.",
    "For beautiful eyes, look for the good in others; for beautiful lips, speak only words of kindness; and for poise, walk with the knowledge that you are never alone.",
    "By three methods we may learn wisdom: First, by reflection, which is noblest; Second, by imitation, which is easiest; and third by experience, which is the bitterest.",
    "If you\'re trying to achieve, there will be roadblocks. I\'ve had them; everybody has had them. But obstacles don\'t have to stop you. If you run into a wall, don\'t turn around and give up. Figure out how to climb it, go through it, or work around it.",
    "The only true wisdom is in knowing you know nothing.",
    "A wise man is superior to any insults which can be put upon him, and the best reply to unseemly behavior is patience and moderation.",
    "A good head and a good heart are always a formidable combination.",
    "Wisdom begins in wonder.",
    "The teacher who is indeed wise does not bid you to enter the house of his wisdom but rather leads you to the threshold of your mind.",
    "I\'d rather regret the things I\'ve done than regret the things I haven\'t done.",
    "The pessimist complains about the wind; the optimist expects it to change; the realist adjusts the sails.",
    "Honesty is the first chapter in the book of wisdom.",
    "A fool flatters himself, a wise man flatters the fool.",
    "A man must be big enough to admit his mistakes, smart enough to profit from them, and strong enough to correct them.",
    "If you talk to a man in a language he understands, that goes to his head. If you talk to him in his language, that goes to his heart.",
    "It\'s not what you look at that matters, it\'s what you see.",
    "Discipline is the bridge between goals and accomplishment.",
    "Winners never quit and quitters never win.",
    "Reality is merely an illusion, albeit a very persistent one.",
    "Beware of false knowledge; it is more dangerous than ignorance.",
    "When it is obvious that the goals cannot be reached, don\'t adjust the goals, adjust the action steps.",
    "A little knowledge that acts is worth infinitely more than much knowledge that is idle.",
    "Always keep an open mind and a compassionate heart.",
    "Everything has beauty, but not everyone sees it.",
    "Choose a job you love, and you will never have to work a day in your life.",
    "It does not matter how slowly you go as long as you do not stop.",
    "I hear and I forget. I see and I remember. I do and I understand.",
    "It is easy to hate and it is difficult to love. This is how the whole scheme of things works. All good things are difficult to achieve; and bad things are very easy to get.",
    "He who learns but does not think, is lost! He who thinks but does not learn is in great danger.",
    "Do not impose on others what you yourself do not desire.",
    "To see the right and not to do it is cowardice.",
    "The faults of a superior person are like the sun and moon. They have their faults, and everyone sees them; they change and everyone looks up to them.",
    "The superior man acts before he speaks, and afterwards speaks according to his action.",
    "Study the past, if you would divine the future.",
    "What you do not want done to yourself, do not do to others.",
    "Wherever you go, go with all your heart.",
    "Only the wisest and stupidest of men never change.",
    "To practice five things under all circumstances constitutes perfect virtue; these five are gravity, generosity of soul, sincerity, earnestness, and kindness.",
    "Faced with what is right, to leave it undone shows a lack of courage.",
    "To go beyond is as wrong as to fall short.",
    "There are three methods to gaining wisdom. The first is reflection, which is the highest. The second is limitation, which is the easiest. The third is experience, which is the bitterest.The object of the superior man is truth.",
    "Silence is a source of great strength.",
    "Honesty is the best policy.",
    "If you only have a hammer, you tend to see every problem as a nail.",
    "A mistake is simply another way of doing things."
};

#endif // WISDOM_H
