#ifndef MISC_H
#define MISC_H

static const char *stringsMisc[] = {
        "Do not rush into risky business decisions. Think over and ask for advice someone you trust. Spend more time with your partner.",
        "Today is good day for breaking with bad habits.  Go to bed early and seize the next day.",
        "Do what must be done. Delaying decisions cannot be a solution in a long term.",
        "Spend some time with your partner, you have been neglecting him in the last few days.",
        "You will see an old friend.",
        "Avoid judging about others. That will distract you from your goals.",
        "Try to implement some sport or other activities in your day schedule, you definitely need a break from your work routines.",
        "You think way too much in the last few days, rely on your feelings instead.",
        "Today is good idea to face a new challenge.",
        "Take a day off and spend some time with your friends.",
        "Give your advice only if you asked to do that.",
        "Today is good day to cut off with burdens from the past.",
        "Talk to a friend, you will feel better.",
        "Enjoy the day. Do not focus on one thing.",
        "Do not reach out for help,you are able to solve your current problems on your own, you will benefit greatly doing that.",
        "Anger is bad advisor, calm down first, take your time before taking decisions.",
        "The planet\'s constellation favours your business activities.",
        "Do not get tempted to express your opinion, you will put yourself in an unpleasant situation.",
        "Prepare yourself good and you will be satisfied with the results.",
        "Spare a hour daily on your project, you will be surprised by the outcomings.",
        "Do no hesitate to correct a mistake that you recently made. Time will not make it look better.",
        "Pay some attention to your day routine, your time management is poor, there is  room for improvement.",
        "Be thankful for what you get. Lower your expectations.",
        "Do not hesitate for asking help and advice. You will be helped.",
        "You are having a hard time. Take a break and spend some time in the nature.",
        "Take your time for that decision, sleep over it and talk with your family about it.",
        "Not a good time to make important decisions.",
        "Be yourself, you cannot be anybody else.",
        "Try at least for one day to be the person that your dog think you are.",
        "If you are having trouble with your business associates, it is a good time to try a new methods of persuation."
};

#endif // MISC_H
