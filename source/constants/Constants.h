#ifndef CONSTANTS_H
#define CONSTANTS_H

#include <cstddef>

#include "strings/Friendship.h"
#include "strings/Happiness.h"
#include "strings/Love.h"
#include "strings/Wisdom.h"
#include "strings/Misc.h"

enum CookieType
{
	CookieOfTheDay,
	Friendship,
	Happiness,
	Love,
	Wisdom,
};

// Returns the size of an array
template <typename T,size_t S>
inline size_t arraysize(const T (&v)[S]) { return S; }

// Returns the size of an array
//template <typename T,unsigned S>
//inline unsigned arraysize(const T (&v)[S]) { return S; }

#endif