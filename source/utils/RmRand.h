#ifndef RMRAND_H
#define RMRAND_H

static unsigned long int next = 1;

int rmRand(void) // RAND_MAX assumed to be 32767
{
    next = next * 1103515245 + 12345;
    return (unsigned int)(next/65536) % 32768;
}

void rmSrand(unsigned int seed)
{
    next = seed;
}

#endif