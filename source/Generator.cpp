#include "Generator.h"

#include <stdlib.h>
#include <time.h>

//extern "C" {
#include "constants/Constants.h"
//}


Generator::Generator(void)
{
	
}

Generator::~Generator(void)
{
}

const char* Generator::makePrediction(CookieType cookieType)
{
	time_t currentRawTime = time(NULL);
    struct tm *currentTime;
    currentTime = localtime(&currentRawTime);

	const char** strings;

	srand(currentTime->tm_mday);
	int localCookieType = rand() % 4 + 1;

	switch (cookieType)
	{
	case CookieOfTheDay:
		switch (localCookieType)
		{
		case Friendship:
			strings = stringsFriendship;
			//this->stringsLength = sizeof (stringsFriendship) / sizeof (char*);
			this->stringsLength = arraysize(stringsFriendship);
			srand(currentTime->tm_mday + cookieType + 11);
			break;
		case Happiness:
			strings = stringsHappiness;
			//this->stringsLength = sizeof (stringsHappiness) / sizeof (char*);
			this->stringsLength = arraysize(stringsHappiness);
			srand(currentTime->tm_mday + cookieType + 433);
			break;
		case Love:
			strings = stringsLove;
			//this->stringsLength = sizeof (stringsLove) / sizeof (char*);
			this->stringsLength = arraysize(stringsLove);
			srand(currentTime->tm_mday + cookieType + 1487);
			break;
		case Wisdom:
			strings = stringsWisdom;
			//this->stringsLength = sizeof (stringsWisdom) / sizeof (char*);
			this->stringsLength = arraysize(stringsWisdom);
			srand(currentTime->tm_mday + cookieType + 4463);
			break;
		}
		break;
	case Friendship:
		strings = stringsFriendship;
		//this->stringsLength = sizeof (stringsFriendship) / sizeof (char*);
		this->stringsLength = arraysize(stringsFriendship);
		srand(currentTime->tm_mday + cookieType + 11);
		break;
	case Happiness: 
		strings = stringsHappiness;
		//this->stringsLength = sizeof (stringsHappiness) / sizeof (char*);
		this->stringsLength = arraysize(stringsHappiness);
		srand(currentTime->tm_mday + cookieType + 433);
		break;
	case Love: 
		strings = stringsLove;
		//this->stringsLength = sizeof (stringsLove) / sizeof (char*);
		this->stringsLength = arraysize(stringsLove);
		srand(currentTime->tm_mday + cookieType + 1487);
		break;
	case Wisdom: 
		strings = stringsWisdom;
		//this->stringsLength = sizeof (stringsWisdom) / sizeof (char*);
		this->stringsLength = arraysize(stringsWisdom);
		srand(currentTime->tm_mday + cookieType + 4463);
		break;
	}

	return strings[rand() % 30];
}