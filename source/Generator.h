#ifndef GENERATOR_H
#define GENERATOR_H

#include "constants/Constants.h"

class Generator
{
public:
	Generator(void);
	~Generator(void);

	const char* makePrediction(CookieType cookieType);
private:
	int stringsLength;
};

#endif
