FortuneCookie
=============

Current build: 1.0.0.0

Introduction
-------------
none of your business


Revisions
=========

Version 1.0
-----------
 + Initial version
 
Known issues
------------
 + none of your business
 

License notes
=============

Graphic assets
--------------
The graphic assets are not free and therefore it is not allowed to be 
downloaded, copied, redistributed or used in any other way.

Notes
=====

none of your business